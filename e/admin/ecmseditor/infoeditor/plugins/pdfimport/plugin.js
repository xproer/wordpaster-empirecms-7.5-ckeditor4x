﻿CKEDITOR.plugins.add('pdfimport',
{
	init: function(editor)
	{
		editor.addCommand('pdfimport',
		{
			exec: function(editor)
            {
                WordPaster.getInstance().SetEditor(editor);
				WordPaster.getInstance().ImportPDF();
			}
		});
		editor.ui.addButton('pdfimport',
		{
			label: '一键导入pdf',
			command: 'pdfimport',
			icon: this.path + 'images/pdf.png'
		});
	}
});
