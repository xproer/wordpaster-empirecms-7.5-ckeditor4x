﻿CKEDITOR.plugins.add('pptpaster',
{
	init: function(editor)
	{
		editor.addCommand('pptpaster',
		{
			exec: function(editor)
			{
				WordPaster.getInstance().PastePPT();
			}
		});
		editor.ui.addButton('pptpaster',
		{
			label: 'PowerPoint一键粘贴',
			command: 'pptpaster',
			icon: this.path + 'images/ppt.png'
		});
	}
});
