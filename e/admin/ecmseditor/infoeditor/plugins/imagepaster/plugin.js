﻿CKEDITOR.plugins.add('imagepaster',
{
	init: function(editor)
	{
		editor.addCommand('imagepaster',
		{
			exec: function(editor)
			{
				WordPaster.getInstance().PasteManual();
			}
		});
		editor.ui.addButton('imagepaster',
		{
			label: 'Word一键粘贴',
			command: 'imagepaster',
			icon: this.path + 'images/paster.png'
		});
	}
});
